
class Customer {

	constructor(email){
		this.email = email
		this.cart = new Cart()
		this.orders = []
	}

}

class Product {

	constructor(name, price){
		this.name = name
		this.price = price
		this.isActive = true
	}

	archive(){
		if(this.isActive === true){
			this.isActive = false
		}else{
			console.log("This item is alreaady archived")
		}
		return this
	}

	updatePrice(price){
		this.price = price
		return this
	}

}



class Cart {

	constructor(contents, quantity){
		this.contents = []
		this.product = new Product()
		this.quantity = quantity
	}

	addToCart(product, quantity){
		this.quantity = quantity
		this.product = product

		let obj = {
			product: this.product,
			quantity: this.quantity
		}
		
		this.contents.push(obj)
		return this

	}

	showCartContents(){
		console.log(this.contents)
		return this
	}

	updateProductQuantity(name, quantity){

		for(let i = 0; i < this.contents.length; i++){
			if(this.contents[i].product.name === name){
				this.contents[i].quantity = quantity
			}
		}
		return this	
	}

}



const john = new Customer("john@mail.com")
const prodA = new Product('soap', 9.99)